# Archiver exporter

This exporter uses the  web interface of the archiver to gather data.

## How to run

To run the application see the provided docker-compose.yml file.
Remember to change the ARCHIVER_URL env variable.


## License

BSD 2-clause
