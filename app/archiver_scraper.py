import requests
import logging
from metrics import Metrics

logger = logging.getLogger("archiver-exporter")


def parse(data):
    metrics = []
    global_labels = {}
    metric_parsed_count = 0
    metric_skipped_count = 0

    for element in data:
        name_u = str(element['name'])  # unparsed name
        value_str = element['value']
        try:
            value_float = float(element['value'].replace(",", ''))
        except ValueError:
            value_float = None
        labels = {'source': element['source']}
        skip = False

        value = value_float
        match name_u:
            case "Appliance Identity":
                global_labels['appliance_identity'] = value_str
                skip = True
            case "Total PV count":
                name = 'pv_count'
            case "Disconnected PV count":
                labels['status'] = "disconnected"
                name = 'pv_count'
            case "Connected PV count":
                labels['status'] = "connected"
                name = 'pv_count'
            case "Paused PV count":
                labels['status'] = "paused"
                name = 'pv_count'
            case "Total channels":
                name = 'channels'
            case "Approx pending jobs in engine queue":
                name = 'pending_jobs'
            case "Event Rate (in events/sec)":
                name = 'event_rate_per_seconds'
            case "Data Rate (in bytes/sec)":
                name = 'data_rate_bytes'
            # Data rate GB/y and GB/day can be calculated on the fly
            # case "Data Rate (in GB/year)":
            #     labels['unit'] = 'GB/year' # FIXME!
            # case "Data Rate (in GB/day)":
            case "Time consumed for writing samplebuffers to STS (in secs)":
                name = 'samplebuffer_time_total'
                labels['destination'] = 'STS'
            case "Benchmark - writing at (events/sec)":
                name = 'benchmark_writing_events_seconds'
            case "Benchmark - writing at (MB/sec)":
                name = 'benchmark_writing_bytes_seconds'
                value = value_float / 1024 / 1024
            case "PVs pending computation of meta info":
                labels['status'] = "pending_computation"
                name = 'pv_count'
            case "Total number of reference counted channels":
                name = 'reference_counted_channels_total'  # FIXME
            case "Total number of CAJ channels":
                name = "channels"
                labels['type'] = 'CAJ'
            case "Channels with pending search requests":
                name = "channels"
                labels['status'] = 'pending_serch_request'
                value = float(value_str.split(' ')[0].replace(",", ''))

            case "Total number of ETL(0&raquo;1) runs so far":
                name = "etl_runs"
                labels["direction"] = '0>1'
            case "Average time spent in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'ETL'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Average percentage of time spent in ETL(0&raquo;1)":
                name = "time_spent_per_run_avg_percent"
                labels['by'] = 'ETL'
                labels["direction"] = '0>1'
            case "Approximate time taken by last job in ETL(0&raquo;1) (s)":
                name = "time_spent_last_job"
                labels['by'] = 'ETL'
                labels["direction"] = '0>1'
            case "Estimated weekly usage in ETL(0&raquo;1) (%)":
                name = "estimated_weekly_usage_percent"
                labels['by'] = 'ETL'
                labels["direction"] = '0>1'
            case "Avg time spent by getETLStreams() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'getETLStreams()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by free space checks in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'space_checks'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by prepareForNewPartition() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'prepareForNewPartition()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by appendToETLAppendData() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'appendToETLAppendData()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by commitETLAppendData() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'commitETLAppendData()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by markForDeletion() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'markForDeletion()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by runPostProcessors() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'runPostProcessors()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Avg time spent by executePostETLTasks() in ETL(0&raquo;1) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'executePostETLTasks()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
            case "Estimated bytes transferred in ETL (0&raquo;1)(GB)":
                name = "transfered_bytes"
                labels['by'] = 'executePostETLTasks()'
                labels["direction"] = '0>1'
                labels['in'] = 'ETL'
                value = value_float / 1024 / 1024 / 1024

            case "Total number of ETL(1&raquo;2) runs so far":
                name = "etl_runs"
                labels["direction"] = '1>2'
            case "Average time spent in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'ETL'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Average percentage of time spent in ETL(1&raquo;2)":
                name = "time_spent_per_run_avg_percent"
                labels['by'] = 'ETL'
                labels["direction"] = '1>2'
            case "Approximate time taken by last job in ETL(1&raquo;2) (s)":
                name = "time_spent_last_job"
                labels['by'] = 'ETL'
                labels["direction"] = '1>2'
            case "Estimated weekly usage in ETL(1&raquo;2) (%)":
                name = "estimated_weekly_usage_percent"
                labels['by'] = 'ETL'
                labels["direction"] = '1>2'
            case "Avg time spent by getETLStreams() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'getETLStreams()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by free space checks in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'space_checks'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by prepareForNewPartition() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'prepareForNewPartition()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by appendToETLAppendData() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'appendToETLAppendData()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by commitETLAppendData() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'commitETLAppendData()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by markForDeletion() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'markForDeletion()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by runPostProcessors() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'runPostProcessors()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Avg time spent by executePostETLTasks() in ETL(1&raquo;2) (s/run)":
                name = "time_spent_per_run_avg_seconds"
                labels['by'] = 'executePostETLTasks()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
            case "Estimated bytes transferred in ETL (1&raquo;2)(GB)":
                name = "transfered_bytes"
                labels['by'] = 'executePostETLTasks()'
                labels["direction"] = '1>2'
                labels['in'] = 'ETL'
                value = value_float / 1024 / 1024 / 1024

            case "PVs in archive workflow":
                labels['status'] = "in_archive_workflow"
                name = 'pv_count'
    #        case: "Capacity planning last update":  # Timedate conversion required
            case "Engine write thread usage":
                name = 'engine_write_thead_usage'
            case "Aggregated appliance storage rate (in GB/year)":
                value = value_float * 31.71  # GB/y to bytes/s
                name = 'aggregated_appliance_storage_rate'
            case "Aggregated appliance event rate (in events/sec)":
                name = 'aggregated_appliance_storage_rate'
            case "Aggregated appliance PV count":
                labels['status'] = "aggregated_appliance"
                name = 'pv_count'
            case "Incremental appliance storage rate (in GB/year)":
                value = value_float * 31.71  # GB/y to bytes/s
                name = 'incremental_appliance_storage_rate'
            case "Incremental appliance event rate (in events/sec)":
                name = 'incremental_appliance_storage_rate'
            case "Incremental appliance PV count":
                labels['status'] = "incremental_appliace"
                name = 'pv_count'
            case _:
                skip = True
        if not skip:
            metric_parsed_count += 1
            metrics.append((name, value, labels))
        else:
            logger.debug("Skipping '%s'", name_u)
            metric_skipped_count += 1

    metrics.append(('metrics', metric_parsed_count, {'status': 'parsed'}))
    metrics.append(('metrics', metric_skipped_count, {'status': 'skipped'}))
    return(metrics, global_labels)


def scrape(url):
    try:
        r = requests.get(url)
        if r.status_code != 200:
            raise Exception
        data = r.json()
    except requests.exceptions.RequestException as error:
        logger.exception(error)
        raise Exception

    metrics, global_labels = parse(data)

    metrics_name = list(dict.fromkeys([x[0] for x in metrics]))

    registry = Metrics(namespace='archiver')
    for metric in metrics_name:
        registry.register(metric, "gauge")
    for metric in metrics:
        registry.add_metric(metric[0], metric[1], metric[2] | global_labels)
    return(registry.collect())


if __name__ == "__main__":
    print(scrape(url='http://archiver-01.tn.esss.lu.se:17665/mgmt/bpl/getApplianceMetricsForAppliance?appliance=archiver-01'))
