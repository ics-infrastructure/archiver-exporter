class Metrics:
    """Store metrics and do conversions to PromQL syntax."""

    class _Metric:
        """This is an actual metric entity."""

        def __init__(self, name, value, metric_type, common_labels, labels=None):
            self.name = name
            self.value = float(value)
            self.metric_type = metric_type
            self.common_labels = common_labels
            self.labels = []
            if labels:
                for label_name, label_value in labels.items():
                    label = '{}="{}"'.format(label_name, label_value)
                    self.labels.append(label)

        def __str__(self):
            return "{}{} {}".format(
                self.name,
                "{" + ",".join(self.labels + self.common_labels) + "}",
                self.value,
            )

    def __init__(self, namespace):
        """Init the registry."""
        self._metrics_registry = {}
        self._metric_types = {}
        self._common_labels = []
        self._namespace = namespace

    def add_common_label(self, name, value):
        """Add a label common to all the metrics"""
        self._common_labels.append('{}="{}"'.format(name, value))

    def register(self, name, metric_type):
        """Add a metric to the registry."""
        if self._metrics_registry.get(name) is None:
            self._metrics_registry[name] = []
            self._metric_types[name] = metric_type
        else:
            raise ValueError(f"Metric named {name} is already registered.")

    def add_metric(self, name, value, labels=None):
        """Add a new metric value."""
        collector = self._metrics_registry.get(name)
        if collector is None:
            raise ValueError("Metric named {} is not registered.".format(name))

        metric = self._Metric(
            f"{self._namespace}_{name}",
            value,
            self._metric_types[name],
            self._common_labels,
            labels,
        )
        collector.append(metric)

    def collect(self):
        """Collect all metrics and return."""
        lines = []
        for name, metric_type in self._metric_types.items():
            lines.append("# TYPE {} {}".format(name, metric_type))
            lines.extend(self._metrics_registry[name])
        return "\n".join([str(x) for x in lines]) + "\n"
